#Primera Etapa
FROM node:14.15.4

RUN npm install -g @angular/cli

WORKDIR /usr/src/app

COPY package.json package-lock.json ./

RUN npm install 

COPY . .

CMD ["npm", "run", "start"]


